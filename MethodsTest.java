
public class MethodsTest {
    public static void main (String[]args){
		
        int x=10;
        int thisVariable=3;
        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);
        methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(10,20.4);
        int z=methodNoInputReturnInt();
		System.out.println(z);
		System.out.println(sumSquareRoot(6,3));
		String s1="hello";
		String s2="goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));

    }
	public static void methodTwoInputNoReturn(int a,double b){
		System.out.println("you are in method TwoInputNoReturn");
	}
    public static void methodNoInputNoReturn(){
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x=50;
        System.out.println(x);
		
    }
    public static void methodOneInputNoReturn(int theInput){
        System.out.println("Inside the method one input no return");
        System.out.println(theInput);
		
    }
	public static int methodNoInputReturnInt(){
		return 6;
	}
	public static double sumSquareRoot(int a, int b){
	double returned=Math.sqrt(a+b);
	return returned;
	}		
  

}
